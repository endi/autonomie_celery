[app:main]
use = egg:caerp_celery#worker

pyramid.default_locale_name = fr_FR
pyramid.includes =
                    pyramid_tm
                    pyramid_mailer.debug
                    pyramid_services
                    pyramid_layout
                    pyramid_mako
                    pyramid_chameleon
                    pyramid_celery

####  MAIL FROM WITHIN THE APPLICATION ####
# Add this one to pyramid.includes if you configured a mailbox
# mail.host = mysmtp
# mail.port=587
# mail.username=<mailbox>
# mail.password=<mailbox_password>
# mail.tls=True
# mail.default_sender=<mailbox>
# mail.optout_address=<optout_address>
# mail.bounce_url=<bound_address>

#### DATABASE ACCESS CONFIGURATION ####
sqlalchemy.url = mysql://caerp:caerp@localhost/caerp?charset=utf8
sqlalchemy.encoding=UTF8
# Those variables should be improved to fit your configuration
sqlalchemy.pool_recycle=7200
sqlalchemy.pool_size=100

# Cache configuration used for file caching
cache.data_dir = %(here)s/data/cache/data
cache.regions = default_term, second, short_term, long_term
cache.type = file
cache.second.expire = 1
cache.short_term.expire = 60
cache.default_term.expire = 1
cache.long_term.expire = 20
#### MAKO SPECIFIC CONFIGURATION ####
mako.directories = caerp:templates
mako.imports = from markupsafe import escape_silent
mako.default_filters = escape_silent
mako.module_directory = /tmp/mako_compiled_templates
caerp.depot_path = /tmp/files/

# Path where we store asynchronously generated files
caerp.static_tmp=/tmp/
# The instance name
caerp.instance_name=intranet.local.fr
# Pool where general_ledger and analytical balances are placed for treatment
caerp.parsing_pool_parent = /tmp/celery_pool
# Sysadmin mail address (used to send information messages)
caerp.sysadmin_mail=admin@local.fr

# Storage service persistant les pdfs sur disque (mode production)
# caerp.services.task_pdf_storage_service = caerp.views.task.pdf_storage_service.PdfFileDepotStorageService

[celery]
broker_url = redis://localhost:6379/0
imports =
    caerp_celery.tasks.tasks
    caerp_celery.tasks.mail
    caerp_celery.tasks.csv_import
    caerp_celery.tasks.export
    caerp_celery.tasks.accounting_measure_compute
    caerp_celery.tasks.accounting_parser
    caerp_celery.tasks.notification
    caerp_celery.tasks.json_import
    caerp_celery.tasks.rgpd

task_serializer = json
accept_content=
                json
                yaml

[celerybeat:accounting_parser]
task = caerp_celery.tasks.accounting_parser.handle_pool_task
type = timedelta
schedule = {"seconds": 30}

[celerybeat:user_connections_clean]
task = caerp_celery.tasks.user_connections.clean_old_user_connections_task
type = crontab
schedule = {"day_of_week": 0, "hour": 12, "minutes": 0}

[celerybeat:check_rgpd_userdata]
task = caerp_celery.tasks.rgpd.check_rgpd_userdata
type = crontab
schedule = {"day_of_week": 0, "hour": 13, "minutes": 0}

[celerybeat:check_rgpd_customers]
task = caerp_celery.tasks.rgpd.check_rgpd_customers
type = crontab
schedule = {"day_of_week": 0, "hour": 13, "minutes": 30}


[celerybeat:check_unused_logins]
task = caerp_celery.tasks.rgpd.check_unused_logins
type = crontab
schedule = {"day_of_week": 0, "hour": 13, "minutes": 30}

## Uncomment the task below to activate daily measures compilation for synchronized uploads
# [celerybeat:accounting_measure_compute]
# task = caerp_celery.tasks.accounting_measure_compute.scheduled_compile_measure_task
# type = crontab
# schedule = {"minute": 0, "hour": 6}

[celerybeat:publish_pending_notifications]
task = caerp_celery.tasks.notification.publish_pending_notifications
type = timedelta
schedule = {"hours": 12}

[celerybeat:clean_notifications]
task = caerp_celery.tasks.notification.clean_notifications
type = timedelta
schedule = {"days": 2}


[server:main]
use = egg:waitress#main
host = 0.0.0.0
#host= 0.0.0.0
port = 8080

[pshell]
models = caerp_celery.models
db=caerp_base.models.base.DBSESSION
transaction=transaction

# Begin logging configuration
[loggers]
keys = root, sqlalchemy, celery

[handlers]
keys = console, file, sqlfile, celery_file

[formatters]
keys = generic, sqlfile

[logger_root]
level = DEBUG
handlers = file, console
propagate = 1

[logger_caerp_celery]
level=DEBUG
handlers=file, console
qualname=caerp_celery

[logger_sqlalchemy]
level = INFO
handlers = file, sqlfile
qualname = sqlalchemy.engine
propagate=0
# level = INFO logs SQL queries.
# level = DEBUG logs SQL queries and results.
# level = WARN logs neither. (Recommended for production systems.)

[logger_celery]
level=DEBUG
handlers = celery_file, console
propagate=1
qualname = celery

[handler_console]
class = StreamHandler
args = (sys.stderr,)
level = NOTSET
formatter = generic

[handler_file]
class = FileHandler
args = ('logfile.log','a')
level = DEBUG
formatter = generic

[handler_celery_file]
class = FileHandler
args = ('celery.log','a')
level = INFO
formatter = generic

[handler_sqlfile]
class = FileHandler
args = ('sql.log','a')
level = DEBUG
formatter = sqlfile

[formatter_sqlfile]
format = %(message)s

[formatter_generic]
format = %(asctime)-15s %(levelname)-5.5s %(message)s ([%(name)s.%(funcName)s:%(lineno)d])
# End logging configuration
